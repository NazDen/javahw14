package happyfamily;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

public class FamilyService {

  private FamilyDao familyDao= new CollectionFamilyDao();

    public FamilyService() {

    }
    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies(){
      return familyDao.getAllFamilies();
  }

  public void displayAllFamilies(){
      System.out.println(familyDao.getAllFamilies());
    }

    public List<Family> getFamiliesBiggerThan(int numberOfMembers){
        List<Family> familiesWithNumberOfMembersBiggerThan = new ArrayList<>();
        for (Family family:
        familyDao.getAllFamilies()) {
            if (family.countFamily()>numberOfMembers){
                familiesWithNumberOfMembersBiggerThan.add(family);
            }
        }
        System.out.println(familiesWithNumberOfMembersBiggerThan);
        return familiesWithNumberOfMembersBiggerThan;
    }

    public List<Family> getFamiliesLessThan(int numberOfMembers){
        List<Family> familiesWithNumberOfMembersLessThan = new ArrayList<>();
        for (Family family:
                familyDao.getAllFamilies()) {
            if (family.countFamily()<numberOfMembers){
                familiesWithNumberOfMembersLessThan.add(family);
            }
        }
        System.out.println(familiesWithNumberOfMembersLessThan);
        return familiesWithNumberOfMembersLessThan;
    }

    public  int countFamiliesWithMemberNumber(int numberOfMembers) {
        int temp = 0;
        for (Family family :
                familyDao.getAllFamilies()) {
            if (family.countFamily() == numberOfMembers) {
                temp++;
            }
        }
        return temp;
    }

    public void createNewFamily(Women mother, Man father){
        Family family = new Family(mother, father);
        familyDao.saveFamily(family);
    }

    public void deleteFamilyByIndex(int index){
      familyDao.deleteFamily(index);
    }

    public Family bornChild(Family family,String boyName,String girlName){

      familyDao.getFamilyByIndex(familyDao.getAllFamilies().indexOf(family)).getMother().bornChild(boyName, girlName);
      familyDao.saveFamily(family);
      return family;
  }

   public Family adoptChild(Family family,Human human){
       familyDao.getFamilyByIndex(familyDao.getAllFamilies().indexOf(family)).addChild(human);
       familyDao.saveFamily(family);
       return family;
   }

   public void deleteAllChildrenOlderThen(int age){
       ListIterator<Family> families=familyDao.getAllFamilies().listIterator();
       while (families.hasNext()) {
           Family tempFamily= families.next();
           ListIterator<Human> children= tempFamily.getChildren().listIterator();
           while (children.hasNext()) {
               Human child= children.next();
               if (age <child.getBirthDate()){
                   children.remove();
               }
           }
           familyDao.saveFamily(tempFamily);
       }
   }

   public int count(){
     return familyDao.getAllFamilies().size();
   }

   public Family getFamilyById(int index){
      return familyDao.getFamilyByIndex(index);
   }

   public Set getPets(int indexOfFamily){
    return familyDao.getFamilyByIndex(indexOfFamily
    ).getPet();
   }

   public void addPet(int indexOfFamily, Pet pet){
      familyDao.getFamilyByIndex(indexOfFamily).getPet().add(pet);
    }



}
